﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcPhoneDirectory.Models;

namespace MvcPhoneDirectory.Controllers
{
    public class SubscriberController : Controller
    {
        private readonly MvcPhoneDirectoryContext _context;

        public SubscriberController(MvcPhoneDirectoryContext context)
        {
            _context = context;
        }

        // GET: Subscriber
        public async Task<IActionResult> Index(SortState sortContact = SortState.LastNameAsc)
        {
            IQueryable<Subscriber> subscribers = _context.Subscriber;

            ViewData["LastNameSort"] = sortContact == SortState.LastNameAsc ? SortState.LastNameDesc : SortState.LastNameAsc;
            ViewData["FirstNameSort"] = sortContact == SortState.FirstNameAsc ? SortState.FirstNameDesc : SortState.FirstNameAsc;
            ViewData["PatronymicSort"] = sortContact == SortState.PatronymicAsc ? SortState.PatronymicDesc : SortState.PatronymicAsc;

            switch (sortContact)
            {
                case SortState.LastNameDesc:
                    subscribers = subscribers.OrderByDescending(p => p.LastName);
                    break;
                case SortState.FirstNameAsc:
                    subscribers = subscribers.OrderBy(p => p.FirstName);
                    break;
                case SortState.FirstNameDesc:
                    subscribers = subscribers.OrderByDescending(p => p.FirstName);
                    break;
                case SortState.PatronymicAsc:
                    subscribers = subscribers.OrderBy(p => p.Patronymic);
                    break;
                case SortState.PatronymicDesc:
                    subscribers = subscribers.OrderByDescending(p => p.Patronymic);
                    break;
                default:
                    subscribers = subscribers.OrderBy(p => p.LastName);
                    break;
            }

            return View(await subscribers.ToListAsync());
            
            /* Вывод отсортированных значений по 3 полям
             * return View(await _context.Subscriber
                 .OrderBy(p => p.LastName)
                 .ThenBy(P => P.FirstName)
                 .ThenBy(P => P.Patronymic)
                 .ToListAsync());*/
        }

        // GET: Subscriber/Details/id
        public async Task<IActionResult> Details(int? id)
        {
            //Получаю человека
            if (id == null)
            {
                return NotFound();
            }

            var subscriber = await _context.Subscriber
                .FirstOrDefaultAsync(m => m.ID == id);
            if (subscriber == null)
            {
                return NotFound();
            }

            //Получаю его номера  
            List<PhoneNumber> numbers = new List<PhoneNumber>();

            await _context.PhoneNumber.ForEachAsync(p =>
           {
               if (p.SubscriberId == id)
               {
                   numbers.Add(p);
               }
           });

            ViewBag.numbers = numbers;

            return View(subscriber);
        }

        // GET: Subscriber/CreateSubscriber
        public IActionResult CreateSubscriber()
        {
            return View();
        }

        // POST: Subscriber/CreateSubscriber
        [HttpPost, ActionName("CreateSubscriber")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateSubscriberConfirmed([Bind("Id,LastName,FirstName,Patronymic")] Subscriber subscriber, string number)
        {
            if (ModelState.IsValid)
            {
                _context.Add(subscriber);

                PhoneNumber phoneNumber = new PhoneNumber
                {
                    Number = number,
                    SubscriberId = subscriber.ID
                };

                _context.PhoneNumber.Add(phoneNumber);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Subscriber/CreateNumber
        public IActionResult CreateNumber(int? id)
        {
            return View();
        }

        // POST: Subscriber/CreateNumber
        [HttpPost, ActionName("CreateNumber")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateNumberConfirmed(string number, int id)
        {
            if (ModelState.IsValid)
            {
                PhoneNumber phoneNumber = new PhoneNumber
                {
                    Number = number,
                    SubscriberId = id
                };
                _context.Add(phoneNumber);
                await _context.SaveChangesAsync();

                return RedirectToAction("Details", new { @id = id });
            }
            return View();
        }

        // GET: Subscriber/DeleteSubscriber/id
        public async Task<IActionResult> DeleteSubscriber(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscriber = await _context.Subscriber
                .FirstOrDefaultAsync(m => m.ID == id);
            if (subscriber == null)
            {
                return NotFound();
            }

            return View(subscriber);
        }

        // POST: Subscriber/DeleteSubscriber/id
        [HttpPost, ActionName("DeleteSubscriber")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteSubscriberConfirmed(int id)
        {
            var subscriber = await _context.Subscriber.FindAsync(id);
            _context.Subscriber.Remove(subscriber);

            await _context.PhoneNumber.ForEachAsync(p =>
            {
                if (p.SubscriberId == id)
                {
                    _context.PhoneNumber.Remove(p);
                }
            });
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: PhoneNumber/DeleteNumber/id
        public async Task<IActionResult> DeleteNumber(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var number = await _context.PhoneNumber
                .FirstOrDefaultAsync(m => m.ID == id);
            if (number == null)
            {
                return NotFound();
            }

            return View(number);
        }

        // POST: PhoneNumber/DeleteNumber/id
        [HttpPost, ActionName("DeleteNumber")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteNumberPost(int id)
        {
            var number = await _context.PhoneNumber.FindAsync(id);
            _context.PhoneNumber.Remove(number);

            await _context.SaveChangesAsync();
            return RedirectToAction("Details", new { @id = number.SubscriberId });
        }

        private bool SubscriberExists(int id)
        {
            return _context.Subscriber.Any(e => e.ID == id);
        }
    }
}
