﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MvcPhoneDirectory.Models
{
    public class MvcPhoneDirectoryContext : DbContext
    {
        public MvcPhoneDirectoryContext (DbContextOptions<MvcPhoneDirectoryContext> options)
            : base(options)
        {
        }

        public DbSet<Subscriber> Subscriber { get; set; }
        public DbSet<PhoneNumber> PhoneNumber { get; set; }
    }
}
