﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcPhoneDirectory.Models
{
    public class Subscriber
    {

        [Key]
        public int ID { get; set; }

        [Required, Display(Name = "Фамилия"), StringLength(60, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required, Display(Name = "Имя"), StringLength(60, MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required, Display(Name = "Отчество"), StringLength(60, MinimumLength = 2)]
        public string Patronymic { get; set; }

        [Display(Name = "Номер")]
        public ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
    public enum SortState
    {
        LastNameAsc,  
        LastNameDesc, 
        FirstNameAsc, 
        FirstNameDesc,
        PatronymicAsc,
        PatronymicDesc
    }
}
