﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcPhoneDirectory.Models
{
    public class PhoneNumber
    {

        [Key]
        public int ID { get; set; }

        [Required, Display(Name = "Номер")]
        
        public string Number { get; set; }

        [Required]
        public int? SubscriberId { get; set; }

        [Required, Display(Name = "Владелец")]
        public Subscriber Subscriber { get; set; }
    }
}
